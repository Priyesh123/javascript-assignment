class Person{
    constructor(name,age,salary,sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
      }

      static sorted(PersonArr,fieldName,order)
      {
          var result = [];
          if(fieldName == 'name'){
              if(order == 'asc'){
                  result = this.quickSortascOnName(PersonArr);
              }
              else{
                  result = this.quickSortascOnNamedesc(PersonArr);
              }
          }
          if(fieldName == 'age'){
            if(order == 'asc'){
                result = this.quickSortascOnAge(PersonArr);
            }
            else
            {
                result = this.quickSortascOnAgedesc(PersonArr);
            }
        }

        if(fieldName == 'salary'){
            if(order == 'asc'){
                result = this.quickSortascOnsalary(PersonArr);
            }
            else
            {
                result = this.quickSortascOnsalarydesc(PersonArr);
            }
        }

        if(fieldName == 'gender')
        {
            if(order == 'asc')
            {
                // Place M first
                result = this.quickSortascOngender(PersonArr);
            }
            else{
                // Place F first
                result = this.quickSortascOngenderdesc(PersonArr);
            }
        }
        return result;
      }

      static quickSortascOnName(arr)
      {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].name.localeCompare(pivot.name) <= 0) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat( this.quickSortascOnName(left), pivot , this.quickSortascOnName(right) );
        }
      }

      static quickSortascOnAge(arr)
      {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].age <= pivot.age) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat(this.quickSortascOnAge(left), pivot , this.quickSortascOnAge(right));
        }
    }

    static quickSortascOnsalary(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].salary <= pivot.salary) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat(this.quickSortascOnsalary(left), pivot , this.quickSortascOnsalary(right));
        }
    }

    static quickSortascOngender(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].sex == 'M') {
                    // console.log(arr[i]);
                    left.push(arr[i]);
                } else {
                    // console.log(arr[i]);
                    right.push(arr[i]);
                }
            }

            return newArray.concat(left,right);
        }
    }

    static quickSortascOnNamedesc(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].name.localeCompare(pivot.name) >= 0) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            // console.log(left,right);
            return newArray.concat( this.quickSortascOnNamedesc(left), pivot , this.quickSortascOnNamedesc(right) );
        }
    }

    static quickSortascOnsalarydesc(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].salary >= pivot.salary) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            // console.log(left,pivot, right)
            return newArray.concat(this.quickSortascOnsalarydesc(left),pivot, this.quickSortascOnsalarydesc(right));
        }
    }

    static quickSortascOngenderdesc(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].sex == 'F') {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat(left,right);
        }
    }

    static quickSortascOnAgedesc(arr)
    {
        if (arr.length <= 1) { 
            return arr;
        } else {
            console.log('Entered here');
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                if (arr[i].age >= pivot.age) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat(this.quickSortascOnAgedesc(left), pivot , this.quickSortascOnAgedesc(right));
        }
    }
}



var arr = [];
arr.push(new Person('shyaam',56,10000,'M'));
arr.push(new Person('ramu',65,13000,'M'));
arr.push(new Person('saloni',71,10500,'F'));
arr.push(new Person('simran',72,11000,'F'));
arr.push(new Person('anamika',87,12000,'F'));
arr.push(new Person('siraj',97,9000,'M'));
arr.push(new Person('dhoni',102,12300,'M'));
arr.push(new Person('aman',67,8000,'M'));
arr.push(new Person('sana',33,5000,'F'));
arr.push(new Person('riya',35,4500,'F'));
var newArr = Person.sorted(arr,'salary','asc');
console.log(arr);
console.log(newArr);


// Gender -> done both asc and desc
// salary -> done both asc and desc
// age -> done both asc and desc
// name -> done both asc and desc