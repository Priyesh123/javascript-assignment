const form = document.getElementById('form1');
form.addEventListener('submit', (e) => FormValidation(e,true));


var obj = {};
function FormValidation(event,valid) {
    //1. all fields should be filled
    event.preventDefault();
    if (document.forms.myForm.email.value.length == 0) {
        alert("Enter your emailId");
        valid = false;
    }
    if (document.forms.myForm.password.value.length == 0) {
        alert("Enter your password");
        valid = false;
    }
    if (document.forms.myForm.gender.selectedIndex == 0) {
        alert("Invalid gender");
        valid = false;
    }
    if ((document.forms.myForm.Role[0].checked == false) && (document.forms.myForm.Role[1].checked == false)) {
        alert("Select your role");
        valid = false;
    }

    var countofPerm = 0;
    if(document.forms.myForm.Permission[0].checked == true) {
        countofPerm++;
    }
    if(document.forms.myForm.Permission[1].checked == true) {
        countofPerm++;
    }
    if(document.forms.myForm.Permission[2].checked == true) {
        countofPerm++;
    }
    if(document.forms.myForm.Permission[3].checked == true) {
        countofPerm++;
    }

    if(countofPerm < 2) {
        alert('select atleast 2 permission');
        valid = false;
    }


    // email check
    var x=document.forms.myForm.email.value;  
    var atposition=x.indexOf("@");  
    var dotposition=x.lastIndexOf("."); 
    if(atposition < 1 || dotposition == atposition+1 || dotposition == x.length-1){
        alert("Invalid email")
        valid = false;
    }

    // Password check
    var pass = document.forms.myForm.password.value;
    var len = pass.length;
    var countCcl = 0;
    var countCsl = 0;
    var countdig = 0;
    for (var i = 0; i < len; i++) {
        if (pass.charAt(i) >= 'A' && pass.charAt(i) <= 'Z') {
            countCcl++;
        }
        else if (pass.charAt(i) >= 'a' && pass.charAt(i) <= 'z') {
            countCsl++;
        }
        else if (pass.charAt(i) >= '0' && pass.charAt(i) <= '9') {
            countdig++;
        }
    }
    console.log(countCcl, countCsl, countdig);
    if (len < 6 || countCcl == 0 || countCsl == 0 || countdig == 0) {
        alert("Invalid Password");
        valid = false;
    }

    obj.email = document.forms.myForm.email.value;
    obj.password = document.forms.myForm.password.value;
    obj.gender = document.forms.myForm.gender.selectedIndex;
    obj.Role = (document.forms.myForm.Role[0].checked == true) ? document.forms.myForm.Role[0].value : document.forms.myForm.Role[1].value;
    obj.PermissionCount = countofPerm;
    console.log(valid);
    if(valid == true){
        // document.getElementById('form1').style.display = 'none';
        console.log(obj);
        document.getElementById('form1').innerHTML = JSON.stringify(obj);
        let brele = document.createElement('br');
        document.getElementById('form1').appendChild(brele);
        let buttonEle = document.createElement('button');
        buttonEle.innerHTML = 'Confirm';
        document.getElementById('form1').appendChild(buttonEle);
        buttonEle.onclick = ()=>{
            window.location.reload();
        }
    }
}





